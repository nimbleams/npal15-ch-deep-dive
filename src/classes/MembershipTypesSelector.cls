/**
 * @description This class is used to query Membership Types.
 */
public abstract class MembershipTypesSelector extends SObjectSelector {

    /**
     * @description Queries Membership Types by Id.
     * @param idSet A set of Ids.
     * @return List of Membership Types.
     */
    public virtual List<NU__MembershipType__c> selectById(Set<Id> idSet) {
        return (List<NU__MembershipType__c>)super.selectSObjectsById(idSet);
    }

    public Schema.SObjectType getSObjectType() {
        return NU__MembershipType__c.SObjectType;
    }

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            NU__MembershipType__c.Name
        };
    }

    // private constructor --> Use newInstance()
    private MembershipTypesSelector() { }

    /**
     * @description Creates the correct with/without sharing instance of this class based on 
     *              whether or not the current user is a community user.
     * @return An instance of the MembershipTypesSelector.
     */
    public static MembershipTypesSelector newInstance() {
        return SystemUtil.isCommunityUser() ? (MembershipTypesSelector)new WithoutSharing() : (MembershipTypesSelector)new WithSharing();
    }

    public without sharing class WithoutSharing extends MembershipTypesSelector {
        public override List<NU__MembershipType__c> selectById(Set<Id> idSet) { return super.selectById(idSet); }
    }

    public with sharing class WithSharing extends MembershipTypesSelector {
        public override List<NU__MembershipType__c> selectById(Set<Id> idSet) { return super.selectById(idSet); }
    }
}