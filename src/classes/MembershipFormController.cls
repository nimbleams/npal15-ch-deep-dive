/**
 * @description Controller class for forms that are used during the Member Join/Renew process.
 */
global virtual class MembershipFormController extends NC.FieldSetFormComponentController {

    // class constants
    private static final String ID_URL_PARAM = 'Id';

    /**
     * @description Determines whether or not this card should be displayed. 
     *              This card will render if a membership type is selected and 
     *              this card's field set is required for the selected membership type.
     * @return A Boolean.
     */
    public override Boolean getShouldRender() {
        return isMembershipTypeSelected() && isInfoRequired();
    }

    // Checks the 'Id' url param to see if it is not null and is for a membership type record.
    private Boolean isMembershipTypeSelected() {
        Id typeId = getMembershipTypeId();

        if (typeId == null) {
            return false;
        }

        return typeId.getSobjectType() == NU__MembershipType__c.SObjectType;
    }

    // Gets the url parameter that represents the selected membership type Id.
    private Id getMembershipTypeId() {
        String membershipTypeIdParam = ApexPages.currentPage().getParameters().get(ID_URL_PARAM);

        if (String.isBlank(membershipTypeIdParam)) {
            return null;
        }

        return Id.valueOf(membershipTypeIdParam);
    }

    // Call the MemberInfoService.cls to see if this card's field set is required for the selected membership type.
    private Boolean isInfoRequired() {
        MemberInfoService.Request request = new MemberInfoService.Request(getMembershipTypeId(), getCard().FieldSetName);
        return MemberInfoService.Instance.isInfoRequired(request);
    }
}