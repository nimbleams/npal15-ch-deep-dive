/**
 * @description This class is used to determine whether or not a membership type requires certain account information during the join/renew process.
 */
public class MemberInfoService {
	
    // class constants.
    @testVisible private static final String REQUEST_PARAM = 'request';
    @testVisible private static final String FIELD_SET_NAME_PARAM = 'fieldSetName';
    @testVisible private static final String TYPE_ID_PARAM = 'typeId';

    // instance variables.
    private Map<String, MemberInfoSetting> settingsByFieldSet;
    private Map<Id, NU__MembershipType__c> typesById;

    // private constructor --> use static instance property.
    private MemberInfoService() {
        typesById = new Map<Id, NU__MembershipType__c>();
        settingsByFieldSet = groupSettingsByFieldSet();
    }

    /**
     * @description Instance property for getting the singleton instance of this class.
     * @return An instance of the MemberInfoService class
     */
    public static MemberInfoService Instance {
        get {
            if (Instance == null) {
                Instance = new MemberInfoService();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Request class to store the information this service class needs.
     */
    public class Request {

        /**
         * @description The Id of a membership type.
         */
        public Id TypeId { get; private set; }

        /**
         * @description The name of the field set that represents the information that would be collected during join/renew.
         */
        public String FieldSet { get; private set; }

        /**
         * @description Class constructor.
         * @param membershipTypeId The Id of the membership type.
         * @param fieldSet The name of a field set.
         */
        public Request(Id membershipTypeId, String fieldSetName) {
            NC.ArgumentNullException.throwIfNull(membershipTypeId, TYPE_ID_PARAM);
            NC.ArgumentNullException.throwIfBlank(fieldSetName, FIELD_SET_NAME_PARAM);

            this.TypeId = membershipTypeId;
            this.FieldSet = fieldSetName;
        }
    }

    /**
     * @description Determines if a field set should be displayed in the join/renew process for a particular membership type.
     * @param request An instance of the request class for this service.
     * @return True if the specified field set should be displayed for the specified membership type.
     */
    public Boolean isInfoRequired(Request request) {
        NC.ArgumentNullException.throwIfNull(request, REQUEST_PARAM);

        NU__MembershipType__c membershipType = getMembershipType(request.TypeId);
        MemberInfoSetting setting = getSetting(request.FieldSet);

        // if either objects can not be found, return false.
        if (membershipType == null || setting == null) {
            return false;
        }

        return setting.isForAllTypes() || setting.forMembershipType(membershipType.Name);
    }

    // gets MemberInfoSetting__c records, constructs wrapper classes, and groups them by field set name.
    private Map<String, MemberInfoSetting> getSettingsByFieldSet() {
        if (settingsByFieldSet != null) {
            return settingsByFieldSet;
        }

        settingsByFieldSet = groupSettingsByFieldSet();

        return settingsByFieldSet;
    }

    private Map<String, MemberInfoSetting> groupSettingsByFieldSet() {
        List<MemberInfoSetting__c> settingRecords = MemberInfoSetting__c.getAll().values();

        Map<String, MemberInfoSetting> settingsByFieldSetName = new Map<String, MemberInfoSetting>();
        for (MemberInfoSetting__c record : settingRecords) {
            MemberInfoSetting wrapper = new MemberInfoSetting(record);
            settingsByFieldSetName.put(wrapper.getFieldSetName(), wrapper);
        }

        return settingsByFieldSetName;
    }

    // gets a single setting using the field set name.
    private MemberInfoSetting getSetting(String fieldSetName) {
        return getSettingsByFieldSet().get(fieldSetName);
    }

    // gets a membership type record from either the cached records or by querying.
    private NU__MembershipType__c getMembershipType(Id typeId) {
        // before querying, check our cached records.
        NU__MembershipType__c membershipType = typesById.get(typeId);

        if (membershipType != null) {
            return membershipType;
        }

        // query for the membership type and return null if no records are returned.
        List<NU__MembershipType__c> types = Selector.MembershipTypes.selectById(new Set<Id> { typeId });
        if (types.isEmpty()) {
            return null;
        }

        // register queried membership type in cache to save queries.
        membershipType = types[0];
        typesById.put(membershipType.Id, membershipType);

        return membershipType;
    }
}