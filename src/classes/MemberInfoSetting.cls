/**
 * @description This class is used to represent a MemberInfoSetting__c record.
 */
public class MemberInfoSetting {
	
    // class constants
    @testVisible private static final String SETTING_PARAM = 'setting';
    @testVisible private static final String MEMBERSHIP_TYPE_NAME_PARAM = 'membershipTypeName';
    private static final String DELIMITER = ',';

    // instance variables
    private MemberInfoSetting__c record;
    private Set<String> membershipTypeNames;

    /**
     * @description Class constructor.
     * @param setting A MemberInfoSetting__c record.
     */
    public MemberInfoSetting(MemberInfoSetting__c setting) {
        NC.ArgumentNullException.throwIfNull(setting, SETTING_PARAM);

        this.record = setting;
    }

    /**
     * @description Determines if the fields specified by this setting should be collected for all membership types.
     * @return True if this setting is for all membership types.
     */
    public Boolean isForAllTypes() { 
        return this.record.AllTypes__c;
    }

    /**
     * @description Returns the name of the field set specfified by this setting.
     * @return A string.
     */
    public String getFieldSetName() {
        return this.record.FieldSet__c;
    }

    /**
     * @description Gets the names of the membership types this setting applies to.
     * @return A Set<String> of the names of membership types.
     */
    public Set<String> getMembershipTypeNames() {
        if (membershipTypeNames != null) {
            return membershipTypeNames;
        }

        membershipTypeNames = buildStringSet(this.record.MembershipTypes__c);

        return membershipTypeNames;
    }

    /**
     * @description Gets the names of the membership types this setting applies to.
     * @param membershipTypeName The name of the membership type to check.
     * @return True if this setting is configured for the specified membership type.
     */
    public Boolean forMembershipType(String membershipTypeName) {
        NC.ArgumentNullException.throwIfBlank(membershipTypeName, MEMBERSHIP_TYPE_NAME_PARAM);

        return getMembershipTypeNames().contains(membershipTypeName.toLowerCase());
    }

    // splits a string and returns a set of those strings.
    private Set<String> buildStringSet(String delimitedString) {
        if (String.isBlank(delimitedString)) {
            return new Set<String>();
        }

        // split the delimited string.
        List<String> stringList = delimitedString.split(DELIMITER);

        // add strings to a set after converting the case.
        Set<String> stringSet = new Set<String>();
        for (String s : stringList) {
            stringSet.add(s.trim().toLowerCase());
        }

        return stringSet;
    }
}