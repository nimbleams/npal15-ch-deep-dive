public with sharing class SystemUtil {
    private static final String USER_TYPE_GUEST = 'Guest';
    private static final String USER_TYPE_COMMUNITY = 'CspLitePortal';

    @testVisible private static Boolean testIsCommunityUser = false;
    private static Boolean bIsCommunityUser = null;

    public static Type GetType(string name) {
        List<string> split = name.split('\\.');
        Type result;
        if (split.size() == 1) {
            result = Type.forName(null, split[0]);
        } else {
            result = Type.forName(split[0], split[1]);
        }
        return result;
    }

    /**
     * @description Gets if the current logged in user is a community user
     * @return Boolean indicating if the current user is a community user
     */
    public static Boolean isCommunityUser() {
        if (bIsCommunityUser == null) {
            String userType = UserInfo.getUserType();
            bIsCommunityUser = userType == USER_TYPE_GUEST || userType == USER_TYPE_COMMUNITY;
        }

        return bIsCommunityUser || testIsCommunityUser;
    }
}