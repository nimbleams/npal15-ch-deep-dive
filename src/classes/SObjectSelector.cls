/**
 * @description An abstract class that handles the base implementation of SOQL Selection.
 */
public abstract without sharing class SObjectSelector {
    @testVisible private static final String ID_SET_PARAM = 'idSet';
    @testVisible private static final String ACCESS_ERROR_MESSAGE =
            'Read access to {0} is not available to the user attempting access.';
    @testVisible protected final String RELATION_PARAM = 'relation';
    @testVisible protected final String PERIOD = '.';

    protected final String COMMA = ',';
    protected final String ORDER_BY_FALLBACK = 'CreatedDate';

    private static final String SELECT_BY_ID_QUERY = 'SELECT {0} FROM {1} WHERE id in :idSet ORDER BY {2}';

    /**
     * @description Constructs the Selector.
     */
    public SObjectSelector() { }

    /**
     * @description Implement this method to define the default selection
     *              query for the SObject to be queried.
     * @param idSet An id of sets to retrieve.
     * @return List of SObjects.
     */
    public virtual List<SObject> selectSObjectsById(Set<Id> idSet) {
        NC.ArgumentNullException.throwIfNull(idSet, ID_SET_PARAM);
        assertIsAccessible();
        return Database.query(buildQuerySObjectById());
    }

    /**
     * @description Constructs the default SOQL query for this selector.
     * See selectSObjectsById.
     */
    private String buildQuerySObjectById() {
        return String.format(SELECT_BY_ID_QUERY,
            new List<String>{getFieldListString(),getSObjectName(),getOrderBy()});
    }

    /**
     * @description Throws an exception if the SObject indicated by
     *              getSObjectType is not accessible to the current
     *              user (read access).
     */
    @testVisible
    protected void assertIsAccessible()
    {
        if (!getSObjectType().getDescribe().isAccessible()) {
            throw new NC.UnauthorizedAccessException(
                    String.format(ACCESS_ERROR_MESSAGE, new List<String> { getSObjectName() }));
        }
    }

    /**
     * @description Gets the name of the implemented Selector's object.
     * @return The name of the object as a string.
     */
    @testVisible
    protected String getSObjectName() {
        return getSObjectType().getDescribe().getName();
    }

    /**
     * @description Constructs a string representing the default fields to query.
     * @return A string containing the comma-delimited names of the default fields.
     */
    public String getFieldListString() {
        List<String> fieldStrings = new List<String>();
        for (Schema.SObjectField f : getSObjectFieldList()) {
             fieldStrings.add(f.getDescribe().getName());
        }
        return String.join(fieldStrings, COMMA);
    }

    private String orderBy;
    /**
     * @description Controls the default ordering of records returned by the base queries.
     *      Defaults to the name field of the object or CreatedDate if there is none.
     * @return Default ordering of records returned by the base queries.
     **/
    public virtual String getOrderBy() {
        if (orderBy == null) {
            orderBy = getNameField() == null ? ORDER_BY_FALLBACK : getNameField().getDescribe().getName();
        }
        return orderBy;
    }

    private Schema.SObjectField nameField;
    /**
    * @description Gets the name field for the sObject Type.
    *       If the sObject does not have a name field, the method returns null.
    * @return The name field of the sObject. Null if there is no name field.
    */
    private Schema.SObjectField getNameField() {
        if (nameField != null) {
            return nameField;
        }
        for (Schema.SObjectField field : getSObjectType().getDescribe().fields.getMap().values()) {
            if (field.getDescribe().isNameField()) {
                nameField = field;
                break;
            }
        }
        return nameField;
    }

    /**
     * @description Takes in a list of fields, returns a list of their names.
     * @param fieldList
     * @return A set of field names as strings.
     */
    protected Set<String> getFieldListStringSet() {
        Set<String> fieldStrings = new Set<String>();
        for (Schema.SObjectField f : getSObjectFieldList()) {
             fieldStrings.add(f.getDescribe().getName());
        }

        return fieldStrings;
    }

    /**
     * @description Implement this method to inform the base class
     *              of the SObject (custom or standard) to be queried.
     */
    abstract Schema.SObjectType getSObjectType();

    /**
     * @description Implement this method to inform the base class
     *              of the common fields to be queried or listed by
     *              the base class methods.
     */
    abstract List<Schema.SObjectField> getSObjectFieldList();
}