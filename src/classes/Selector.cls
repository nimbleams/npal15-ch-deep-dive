/**
 * @description This class is used to store instances of the various
 *              selectors so that a single instance can be reused in a single execution context.
 */
public class Selector {

    /**
     * @description Contains an instance of the MembershipTypesSelector.
     */
    public static MembershipTypesSelector MembershipTypes {
        get {
            if (MembershipTypes == null) {
                MembershipTypes = MembershipTypesSelector.newInstance();
            }
            return MembershipTypes;
        }
        private set;
    }
}